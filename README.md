# autopatch

* [autopatch](autopatch) is a script that downloads an upstream srpm for either alma or rhel release files (`almalinux-release` or `redhat-release`) and automagically modifies it for release to CERN
* The magic comes from AlmaLinux's [autopatch](https://github.com/AlmaLinux/autopatch-tool) with a [wrapper script](almalinux-autopatch-wrapper.py). Configuration files are located in the [configs](configs) directory
* `--git` will make a git commit in a new branch
* `--rpm` will perform a `rpmbuild`

Examples:

* Build latest `almalinux-release` for ALMA9

```
[morrice@morricedesktop autopatch]$ ./autopatch --distribution alma --release 9
./autopatch --distribution alma --release 9
Working with almalinux-release-9.5-2.el9.src.rpm
Downloading almalinux-release-9.5-2.el9.src.rpm to /tmp/tmpx6vqp53_/upstream
Extracting /tmp/tmpx6vqp53_/upstream/almalinux-release-9.5-2.el9.src.rpm
Cloning CERN almalinux-release repo
Comparing/fixing differences
Running AlmaLinux's autopatch tool
--git was passed, so i've made a commit for you in a new branch
You can see the diff with: git --no-pager -C /tmp/tmpx6vqp53_/cern show HEAD
--rpm was passed, building an rpm for you
srpm: /tmp/tmpx6vqp53_/cern/build/SRPMS/almalinux-release-9.5-2.el9.src.rpm rpms: /tmp/tmpx6vqp53_/cern/build/RPMS/x86_64/{almalinux-gpg-keys-9.5-2.el9.x86_64.rpm,almalinux-release-9.5-2.el9.x86_64.rpm,almalinux-sb-certs-9.5-2.el9.x86_64.rpm,almalinux-repos-9.5-2.el9.x86_64.rpm}
[morrice@morricedesktop autopatch]$ 
```

* Build the latest `almalinux-release` for ALMA8

```
[morrice@morricedesktop autopatch]$ ./autopatch --distribution alma --release 8
Working with almalinux-release-8.10-1.el8.src.rpm
Downloading almalinux-release-8.10-1.el8.src.rpm to /tmp/tmp_e2ff0bs/upstream
Extracting /tmp/tmp_e2ff0bs/upstream/almalinux-release-8.10-1.el8.src.rpm
Cloning CERN almalinux-release repo
Comparing/fixing differences
Running AlmaLinux's autopatch tool
--git was passed, so i've made a commit for you in a new branch
You can see the diff with: git --no-pager -C /tmp/tmp_e2ff0bs/cern show HEAD
--rpm was passed, building an rpm for you
srpm: /tmp/tmp_e2ff0bs/cern/build/SRPMS/almalinux-release-8.10-1.el9.src.rpm rpms: /tmp/tmp_e2ff0bs/cern/build/RPMS/x86_64/{almalinux-release-8.10-1.el9.x86_64.rpm}
[morrice@morricedesktop autopatch]$ 
```
