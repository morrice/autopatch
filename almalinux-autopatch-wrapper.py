#!/usr/bin/python3

import argparse
import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'src'))

import src.tools.rpm
from actions_handler import ConfigReader

parser = argparse.ArgumentParser(
  description="Wrapper script for AlmaLinux's autopatch tool"
)
parser.add_argument(
  '--config',
  type=str,
  help='config file',
  required=True,
)
parser.add_argument(
  '--targetdir',
  type=str,
  help='Target directory',
  required=True,
)
args = parser.parse_args()

config = ConfigReader(args.config)
config.apply_actions(args.targetdir)
